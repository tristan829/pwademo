const cacheVersion = 'v1.1';
const filesToCache = [
    'index.htm',
    'sw.js',
    'main.css',
    'icons/128.png',
    'icons/256.png',
    'icons/512.png',
];


self.addEventListener('install', function (event) {
    //如果希望異動過的程式能夠立即更新，可以在 install 事件內透過 skipWaiting 方法來立即啟用。
    self.skipWaiting();
    console.log('[ServiceWorker] Install');
    console.log(event);

    var offlinePage = new Request('offline.html');
    event.waitUntil(
        // fetch(offlinePage).then(function (response) {
        //     return caches.open('offline2').then(function (cache) {
        //         return cache.put(offlinePage, response);
        //     });
        // }));


    );
    event.waitUntil(
        caches.open(cacheVersion)
            .then(cache => {
                console.log('[ServiceWorker] Caching app shell');
                return cache.addAll(filesToCache);
            })
    );
    // event.waitUntil(
    //     caches.keys()
    //         .then(keyList => {
    //             return Promise.all(keyList.map(key => {
    //                 if (key !== cacheVersion) {
    //                     return caches.delete(key);
    //                 }
    //             }));
    //         })
    // );
}
);

self.addEventListener('activate', event => {
    console.log('[ServiceWorker] activate');
    console.log(event);
    event.waitUntil(clients.claim());
});

self.addEventListener('fetch', function (event) {
    console.log('[ServiceWorker] fetch');
    console.log(event);
    event.respondWith(
        fetch(event.request).catch(function (error) {
            return caches.open('offline2').then(function (cache) {
                return cache.match('offline.html');
            });
        }));
});

self.addEventListener('refreshOffline', function (response) {
    console.log('[ServiceWorker] refreshOffline');
    console.log(response);
    return caches.open('offline2').then(function (cache) {
        return cache.put(offlinePage, response);
    });
});

self.addEventListener('push', function (event) {
    console.log('[ServiceWorker] push');
    console.log(event);
    var data = event.data.json(); var opts = {
        body: data.body,
        icon: data.icon,
        data: {
            url: data.url
        }
    };
    event.waitUntil(self.registration.showNotification(data.title, opts));
});

self.addEventListener('notificationclick', function (event) {
    console.log('[ServiceWorker] notificationclick');
    console.log(event);
    var data = event.notification.data; event.notification.close(); event.waitUntil(
        clients.openWindow(data.url)
    );
});